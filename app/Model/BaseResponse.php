<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model;

/**
 * Description of BaserResponse
 *
 * @author Sydney
 */
class BaseResponse {
    //put your code here
    
    public $status,$message,$data;
    
    public function __construct($status, $message) {
        $this->status = $status;
        $this->message = $message;
       $this->data = new EmptyObjet();
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getData() {
        return $this->data;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }
}
class EmptyObjet{}