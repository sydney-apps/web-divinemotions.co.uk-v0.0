<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model;
/**
 * Description of ContactForm
 *
 * @author Sydney
 */
class ContactForm {
    //put your code here
    public $name, $message, $subject, $category, $email;
    
    public function __construct() {
        
    }
    public function getName() {
        return $this->name;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
        return $this;
    }

    public function setCategory($category) {
        $this->category = $category;
        return $this;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }


}
