<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Request;
use App\Http\Model\ContactFormResponse;
use App\Http\Model\GenericBaseResponse;
use App\Http\Model\BaseResponse;
use Config;
use App\Mail\Webform;
use Illuminate\Support\Facades\View;
/**
 * Description of Contactus
 *
 * @author Sydney
 */
class Contactus extends Controller {

    /**
     * Show the contact for the given user.
     *
     * @return Response
     */
    public function validateForm() {
        // Getting all post data
        $a = array();
        if (Request::ajax()) {
            
            $data = Input::all();
            $name = $data['name'];
            if (iconv_strlen($name) < 2) {
                $errorResponse = new ContactFormResponse('f1', Config::get('constants.form.name'));
                array_push($a, $errorResponse);
            }
            // Field Email
            $email = $data['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorResponse = new ContactFormResponse('f3', Config::get('constants.form.email'));
                array_push($a, $errorResponse);
            }
            // Field Category
            $category = 'webmail';
            $subject ='Webmail';

            // Field Message
            $msg = $data['message'];
            if (iconv_strlen($msg) < 10) {
                
                $errorResponse = new ContactFormResponse('f5', Config::get('constants.form.message'));
                array_push($a, $errorResponse);
            }

            if (count($a) > 1) {
                
                //print_r($message);die;
                $response = new GenericBaseResponse(Config::get('constants.status.validation_failure'), Config::get('constants.msg.validation_failure'), $a);
                return json_encode($response);
            } else {
                // To send HTML mail, the Content-type header must be set
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=UTF-8';

                // Additional headers
                $headers[] = 'To: Admin <info@divinemotions.co.uk>';
                $headers[] = 'From: Divine Healthcare Website <'.$email.'>';
                    
                if (!isset($subject) || empty($subject)) {
                    $subject = Config::get('constants.form.CONTACT_WEBMAIL');
                }
                try
                {
                    /*Mail::send('emails.welcome', $data, function($message)
                    {
                        $message->from('us@example.com', 'Laravel');

                        $message->to('info@sydneyapps.co.uk')->cc('mlambosydney@yahoo.com');

                        $message->attach($pathToFile);
                    });*/
                   /* */
                    
                    $emailcontent = array(
                        'subject' => $data['subject'],
                        'msg' => $data['message'],
                        'category' => $data['category'],
                        'name' => $data['name'],
                        'email'=> $data['email']
                    );
                    /*
                   Mail::send('emails.contacthtml', $emailcontent, function($message) {
                       $message->to('mlambosydney@yahoo.com', 'Receiver Name');
                       $message->subject('This is the subject');
                   });*/
                $view = View::make('emails.contacthtml', $emailcontent)->render();
                
                mail('info@cynosurehcs.co.uk', $subject, $view, implode("\r\n", $headers));
                
                $headers[] = 'MIME-Version: 1.0';
                $headerse[] = 'Content-type: text/html; charset=iso-8859-1';
                // Additional headers
                $headerse[] = 'To: <'.$email.'>';
                $headerse[] = 'From: Cynosure Website <no-reply@cynosurehcs.co.uk>';
                mail($email, 'We received your message below.', $view, implode("\r\n", $headerse));

                } catch (Exception $e) {
                    $response = new BaseResponse(Config::get('constants.status.success'), 'An exception occured.');
                    Log::critical($e->getMessage());
                    return json_encode($response);

                }
                $response = new BaseResponse(Config::get('constants.status.success'), Config::get('constants.msg.success'));
                return json_encode($response);
                
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
