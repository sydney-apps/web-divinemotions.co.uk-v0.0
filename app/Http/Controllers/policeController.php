<?php

namespace App\Http\Controllers;

use App\Model\Downloadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class policeController extends Controller
{
//
    public function index(){
    
        $files = Storage::allFiles('/public/police');

        $downloads = array();
        for($x = 0; $x < count($files); $x++) {          
            $downloads[$x] = new Downloadable( $files[$x]);
        }
        return view('about.police', ['files'=>$downloads]);
    }

}
