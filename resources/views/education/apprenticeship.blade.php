@extends('layouts.page')
@section('page-title')
Divine Motions Aca Care : Education - QCF Training
@endsection
@section('article-title')
Supported Living : Accommodation
        QCF Training
@endsection
@section('article-body')
<article>

<div class="entry-content">
        <p>Apprenticeships are an excellent way of gaining qualifications and workplace experience. As an employee, you can earn as you learn and you gain&nbsp;practical skills from the workplace.</p>
                <button class="accordion">Health and Social Care Apprenticeship</strong><i class="fa fa-plus"></i></button>
        <div class="panel">
                <p>This Apprenticeship covers a wide range of job&nbsp;roles on two pathways: the adult social care&nbsp;pathway and the health care pathway. This could&nbsp;mean working in the NHS, the private sector, local&nbsp;authorities or in the voluntary sectors.</p>
        </div>
        <button class="accordion">Customer Service Apprenticeship</strong><i class="fa fa-plus"></i></button>
        <div class="panel">
                <p>This Apprenticeship teaches you the skills to provide&nbsp;excellent customer service, and can be applied to&nbsp;hundreds of job roles across many different sectors,&nbsp;from government to telecommunications. However,&nbsp;most customer service apprentices work in retail,&nbsp;financial services, call centres, hospitality, or sport and&nbsp;recreation.</p>
        </div>
        <button class="accordion">Business Administration Apprenticeship</strong><i class="fa fa-plus"></i></button>
        <div class="panel">
                <p>This course provides a good grounding for a first time&nbsp;job in an administrative capacity and can be undertaken&nbsp;in a range of work environments. Apprentices&nbsp;will be producing reports and correspondence, working&nbsp;with staff within an organisation and its customers&nbsp;and clients.</p>
                <p>There are loads of opportunities in Business&nbsp;Administration if you’re enthusiastic, motivated and&nbsp;efficient at multi-tasking.</p>
        </div>
</div>
                        
</article>
@endsection