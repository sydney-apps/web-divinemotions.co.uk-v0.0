<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
	
    <!-- css -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/plugins/cubeportfolio/css/cubeportfolio.min.css">
	<link href="/css/nivo-lightbox.css" rel="stylesheet" />
	<link href="/css/nivo-lightbox-theme/default/default.css?v=0.43" rel="stylesheet" type="text/css" />
	<link href="/css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="/css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="/css/animate.css" rel="stylesheet" />
    <link href="/css/style.css?v=0.93" rel="stylesheet">

	<!-- boxed bg -->
	<link id="bodybg" href="/bodybg/bg1.css" rel="stylesheet" type="text/css" />
	<!-- template skin -->
	<link id="t-colors" href="/color/default.css?v=0.43" rel="stylesheet">
	@yield('headerscript')
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

<div id="wrapper">
	
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="top-area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<p><img src="/img/logo-top.png?v=1" alt="" /></p>
					</div>
					<div class="col-sm-6 col-md-6">
					<p class="bold text-right">Call us now +44 (0) 208 665 4334</p>
					</div>
				</div>
			</div>
		</div>

        <div class="container navigation">		
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                    <img src="/img/logo-bottom.gif" alt="" />
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
			  <ul class="nav navbar-nav ">
				<li class="active" ><a href="/">Home</a></li>
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
				<ul class="dropdown-menu">
				    <li><a href="/about">Who we are</a></li>
				    <!-- <li><a href="/downloads/policy">Policies</a></li> -->
					<li><a href="/about/general-information">General Information and Advice</a></li>
					<!--<li><a href="/downloads/cqc">CQC Report</a></li> -->
				  </ul>
				</li>
				<li class=""><a href="#" title="Health Care Services" class="dropdown-toggle" data-toggle="dropdown">Health Care<b class="caret"></b></a>
					<ul  class="dropdown-menu">
							<li class="dropdown-submenu"><a href="/healthcare/services" title="Care Services">Care Services</a>
								<ul class="dropdown-menu">
									<li class=""><a href="/healthcare/recovery-convalescent" title="Recovery/Convalescent Services">Recovery/Convalescent Services</a></li>
									<li class=""><a href="/healthcare/companionship-and-sitting-services" title="Companion/Sitting Services">Companion/Sitting Services</a></li>
									<li class=""><a href="/healthcare/house-keeping" title="Housekeeping Services">Housekeeping Services</a></li>
									<li class=""><a href="/healthcare/personal-care" title="Personal Care">Personal Care</a></li>
								</ul>
							</li>
							<li class="dropdown-submenu"><a href="/healthcare/care-packages" title="Care Packages">Care Packages</a>
								<ul class="dropdown-menu">
									<li class=""><a href="/healthcare/live-in-care" title="Live-in-care">Live-in-care</a></li>
									<li class=""><a href="/healthcare/round-the-clock-care" title="24 Hour Care">24 Hour Care</a></li>
									<li class=""><a href="/healthcare/sleeping-night" title="Sleep Nights / Wake Nights">Sleep Nights / Wake Nights</a></li>
									<li class=""><a href="/healthcare/respite-care" title="Respite Care">Respite Care</a></li>
								</ul>
							</li>
							<li class=""><a href="/healthcare/personalisation" title="Personalisation">Personalisation</a></li>
							<li class="dropdown"><a href="/healthcare/care-funding" title="Care Funding">Care Funding</a>
								<ul class="dropdown-menu">
									<li class=""><a href="/healthcare/social-services-funding" title="Social Services Funding">Social Services Funding</a></li>
									<li class=""><a href="/healthcare/direct-payments" title="Direct Payments">Direct Payments</a></li>
									<li class=""><a href="/healthcare/self-funding" title="Self Funding">Self Funding</a></li>
									<li class=""><a href="/healthcare/nhs-continuing-care" title="NHS Continuing Care">NHS Continuing Care</a></li>
									<li class=""><a href="/healthcare/in-control" title="In Control">In Control</a></li>
									<li class=""><a href="/healthcare/personal-budget" title="Personal Budget">Personal Budget</a></li>
								</ul>
							</li>
							<li class=""><a href="/downloads/healthcare" title="Download Brochure">Download Brochure</a></li>
					</ul>
				</li>
				<li><a href="#" title="Supported Living" class="dropdown-toggle" data-toggle="dropdown" >Supported Living <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class=""><a href="/supported-living/about" title="About our services">About our services</a></li>
						<li class=""><a href="/supported-living/transition-support" title="Transition support">Transition support</a></li>
						<li class=""><a href="/supported-living/outreach-support" title="Outreach support">Outreach support</a></li>
						<li class=""><a href="/supported-living/recovery" title="Recovery">Recovery</a></li>
						<li class=""><a href="/supported-living/accommodation" title="Accommodation">Accommodation</a></li>
						<li class=""><a href="/supported-living/learning-disabilities" title="Learning disabilities">Learning disabilities</a></li>
					</ul>
				</li>
				<!--
				<li class=""><a href="" title="Education" class="dropdown-toggle" data-toggle="dropdown" >Education <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li class=""><a href="/education/qcf-training" title="QCF Training">QCF Training</a></li>
					<li class=""><a href="/education/apprenticeship" title="Apprenticeship">Apprenticeship</a></li>
					<li class=""><a href="/education/loan-for-24-plus" title="Loan  for 24 plus">Loan  for 24 plus</a></li>
					<li class=""><a href="/education/student-support" title="Student Support">Student Support</a></li>
					<li class=""><a href="/downloads/education" title="Download Education Brochure">Download Education Brochure</a></li>
				</ul>
				</li>-->

				<li class="dropdown"><li><a href="/contact-us">Contact Us</a></li>
				<li class="dropdown"><li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="badge custom-badge red pull-right"></span>Recruitement <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="/recruitement/online-application">Online Application</a></li>
				  </ul>
				</li>
			  </ul>
            </div>
        </div>
    </nav>
            @yield('content')
	<section id="partner" class="home-section paddingbot-60">	
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow lightSpeedIn" data-wow-delay="0.1s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Our partner</h2>
					<p>We work with a nummber of proffessional organisations to facilitate quality proffessional services.</p>
					</div>
					</div>
					<div class="divider-short"></div>
				</div>
			</div>
		</div>
		
           <div class="container ">
		   <div class="row ">

		   	<div class="js-imageslider">
			<div class="my-slider-list">
			
			<div class="col-sm-6 col-md-3 my-slider-item">
				<div class="partner">
				<a href="#"><img src="/img/partners/iso.gif" alt="" /></a>
				</div>
			</div>

			<!-- 
			<div class="col-sm-6 col-md-3  my-slider-item">
				<div class="partner">
				<a href="#"><img src="img/partners/citation-gray.gif" alt="" /></a>
				</div>
			</div>
			<div class="col-sm-6 col-md-3 my-slider-item">
				<div class="partner">
				<a href="#"><img src="img/partners/eu-gray.gif" alt="" /></a>
				</div>
			</div>			
			<div class="col-sm-6 col-md-3 my-slider-item">
				<div class="partner">
				<a href="#"><img src="img/partners/ncfe-gray.gif" alt="" /></a>
				</div>
			</div>
			
			
			<div class="col-sm-6 col-md-3 my-slider-item">
				<div class="partner">
				<a href="#"><img src="/img/partners/ocr-gray.gif" alt="" /></a>
				</div>
			</div>-->
			</div>
			</div>
		</div>
        </div>
	</section>	

	<footer>
	
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>About Divinemotions</h5>
						<p>We are egistered Domiciliary Care provider.
						</p>
					</div>
					</div>
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Information</h5>
						<ul>
							<!-- <li><a href="#">CQC Ratings</a></li> -->
							<li><a href="#">Recruitement</a></li>
							<li><a href="#">Education Brochure</a></li>
						</ul>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Contact Us</h5>
						<p>
						Get intouch with us.
						</p>
						<ul>
							
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
								</span> 0208 665 4334
							</li>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
								</span> info@divine-healthcare.uk
							</li>

						</ul>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4">
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<h5>Our location</h5>
						<p>Square Root, Business Centre, 102 – 116, Windmill Road, Croydon CR0 2XQ</p>		
						<ul>
							<li>
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
								</span> Monday - Saturday, 8am to 10pm
							</li></ul>
					</div>
					</div>
					<div class="wow fadeInDown" data-wow-delay="0.1s">
					<div class="widget">
						<!-- <h5>Follow us</h5>
						<ul class="company-social">
								<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="social-google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li class="social-vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
								<li class="social-dribble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
						</ul>
						-->
					</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInLeft" data-wow-delay="0.1s">
					<div class="text-left">
					<p>&copy;Copyright - Divinemotions. All rights reserved.</p>
					</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="wow fadeInRight" data-wow-delay="0.1s">
					<div class="text-right">
						<div class="credits">
							<a href="#">Terms & conditions</a>
                        </div>
					</div>
					</div>
				</div>
			</div>	
		</div>
		</div>
	</footer>

</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<!-- Core JavaScript Files -->
    <script src="/js/jquery.min.js"></script>	 
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
	<script src="/js/wow.min.js"></script>
	<script src="/js/jquery.scrollTo.js"></script>
	<script src="/js/jquery.appear.js"></script>
	<script src="/js/stellar.js"></script>
	<script src="/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
	<script src="/js/owl.carousel.min.js"></script>
	<script src="/js/nivo-lightbox.min.js"></script>
	@yield('scripts')
    <script src="/js/custom.js?v=1.23"></script>    
	<script src="/js/jquery.imageslider.js?v=0.1"></script>
	
	<script>
	$(function() {
		$('.js-imageslider').imageslider({
			slideItems: '.my-slider-item',
			slideContainer: '.my-slider-list',
			slideDistance: 1,
			slideDuration: 1,
			resizable: true,
			pause: true
		});
	});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {

    acc[i].addEventListener("click", function() {
		var icon = this.getElementsByTagName('i')[0];
		icon.classList.toggle("fa-plus")
		icon.classList.toggle("fa-minus")
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
	});
	
}
</script>

</body>

</html>
