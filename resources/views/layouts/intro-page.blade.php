
@extends('layouts.base')

@section('page-title')
@yield('title')
@endsection
@section('content')

	<!-- Section: intro -->
	@section('slider-bar-top')
    <section id="intro" class="intro">
		<div class="intro-content">
			<div class="container">
				<div class="row">
    @show	

	@section('slider-bar-bottom')					
				</div>		
			</div>
		</div>		
    </section>
	@show
	<!-- /Section: intro -->
	@yield('home-content')	
@endsection