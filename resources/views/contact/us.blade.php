	@extends('layouts.intro-page')

	@section('page-title')
	Contact Us - Divine Motions Aca Care
	@endsection
	
	@section('headerscript')
		<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
	
		<script type="text/javascript">
		
			var a = Math.ceil(Math.random() * 10);
			var b = Math.ceil(Math.random() * 10);       
			var c = a + b

			function DrawBotBoot()
			{
				document.write("What is "+ a + " + " + b +"? ");
				
			}    
			
			function ValidBotBoot(){
				var d = document.getElementById('BotBootInput').value;
				if (d == c) return true;        
				return false;
			}

		</script>
	@endsection
	@section('slider-bar-top')
		@parent
		<div class="col-lg-6">
			<div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
				<h2 class="h-ultra">Get in touch</h2>
			</div>
			<div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
				<h4 class="h-light"></h4>
			</div>
			<div class="well well-trans">
				<div class="wow fadeInRight" data-wow-delay="0.1s">
				<ul class="lead-list">
				<li><span class="fa fa-phone fa-2x icon-link"></span> <span class="list"><strong>Contact Number</strong><br />+44 (0) 208 665 4334 </span></li>
				<li><span class="fa fa-envelope fa-2x icon-link"></span> <span class="list"><strong>Email Address</strong><br />info@divine-healthcare.uk</span></li>
				<li><span class="fa fa-calendar fa-2x icon-link"></span> <span class="list"><strong>Open Times</strong><br />Monday - Saturday, 8am to 10pm</span></li>
				</ul>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
						<div class="panel-body">
											
											<form action="" method="post" role="form" id="ajax-email-form" data-url="/contact-validation" class="contactForm">
												<div  class="row">
													<div class="col-xs-12 col-sm-12 col-md-12">
														<div class="form-group">
															<span class="lead-footer">* We'll contact you by phone &amp; email later</span>
															<div id="errormessage"></div>
															<div id="sendmessage">Your message has been sent. Thank you!</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Full Name</label>
															<input type="text" name="first_name" id="f1" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter at least 3 chars">
															<div class="validation"></div>
														</div>
													</div>
													<div class="col-xs-6 col-sm-6 col-md-6">
														<div class="form-group">
															<label>Email</label>
															<input type="text" name="email" id="f3" class="form-control input-md" data-rule="minlen:3" data-msg="Please enter an email">
															<div class="validation"></div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12">
														<div class="form-group">
															<label>Message</label>
															<textarea type="textfield" name="message" id="f5" class="form-control input-md" data-rule="required" data-msg="The message is required"></textarea>
															<div class="validation"></div>
														</div>
													</div>
												</div>
												
												<div class="row">
													<div class="col-xs-6 col-sm-6 col-md-6">
															<label for="f6"><script type="text/javascript">DrawBotBoot()</script></label>
															<input type="text" name="captcha-value" id="BotBootInput" class="form-text"  maxlength='2' size='2' value="" size="40">                                                        

													</div>
													<div class="col-xs-6 col-sm-6 col-md-6">
															<input type="submit" value="Submit" class="btn btn-skin btn-block btn-lg">
													</div>
												</div>
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
											</form>
						</div>
		</div>					
	@endsection
	@section('slider-bar-bottom')
		@parent
		@endsection
	@section('home-content')
	<section id="partner" class="home-section">	
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="home-section">
							<h2 class="h-bold">Our Location </h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="row text-center">
											<div class="col-sm-12">
												<div id="map" class="itited" style="height: 350px;"></div>
												<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5NTW3wmh3NDqkiehT0Ad_4nobp13oMAo"></script>
											</div>
					</div>
				</div>
			</div>
    </section>
    @endsection
	@section('scripts')
		<script type="text/javascript" src="{{ asset('/js/contact_form.js') }}"></script>
	@endsection
