@extends('layouts.page')

@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Recovery/Convalescent Services
@endsection
@section('article-body')
									<div class="entry-content">
											<article id="post-100" class="post-100 page type-page status-publish hentry">
												<div class="entry-content">
													<p>Convalescent and post-operative care is usually a short term requirement while recovering from an illness, accident or hospital stay.<br>
													Divine Motions can provide the care that is needed during this time and help you to get back on your feet.<br>
													We can work alongside healthcare professionals to help, encourage, prompt and assist with rehabilitation exercises, or provide practical help with personal care or housekeeping.</p>
													<h4>Reablement Services</h4>
													<p>Our Re-ablement Services assist Service Users to regain skills they may have lost through poor health, disability, having spent a considerable time in hospital or residential home, frequent admission to hospitals etc. With the help of our qualified and dedicated staff we hope to make your independent living a reality</p>
												</div>
											</article>
									</div>
@endsection