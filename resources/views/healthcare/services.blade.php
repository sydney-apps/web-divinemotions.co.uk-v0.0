@extends('layouts.page')

@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Care Services
@endsection
@section('article-body')
										
									<div class="entry-content">
											<p>We are a registered Domiciliary Care provider, offering Community Supported Living to people living in the community, either with their families or in their own homes.</p>
									<p>We offer support with personal care, as well as help and support for each person to learn new skills, develop independence, access other services and enjoy life as part of the community.</p>
									<p>We provide home based support to individuals who are referred by Social Services or who are purchasing services through Direct Payments or Individual Budgets. Each person has a Support Plan and an Activity Planner. Information includes personal care, learning opportunities, recreational, social, respite breaks, vocational and work activities.</p>
									<p>We provide care and support services to the following Service User groups:</p>
									<button class="accordion"><strong>Mental Health</strong> <i class="fa fa-plus"></i></button>
									<div class="panel">
									Mental Health affects us all. How we think and feel about ourselves and our lives impacts on our behaviour and how we cope in tough times.<br>
									It affects our ability to make the most of the opportunities that come our way and play a full part amongst our family, workplace, community and friends. It’s also closely linked with our physical health.</p>
									<p>Whether we call it well-being, emotional welfare or mental health, it’s key to living a fulfilling life.<br>
									Mental health problems range from the worries we all experience as part of everyday life to serious long-term conditions. The majority of people who experience mental health problems can get over them or learn to live with them, especially if they get help early on.</p>
									<p>Mental health problems are usually defined and classified to enable professionals to refer people for appropriate care and treatment. But some diagnoses are controversial and there is much concern in the mental health field that people are too often treated according to or described by their label. This can have a profound effect on their quality of life. Nevertheless, diagnoses remain the most usual way of dividing and classifying symptoms into groups. At Divine Motions AcaCare Ltd, we provide a range of community based mental health care services to individuals that have been declared safe to come and live in the community.</p>
									<p>The focus of our mental health care services is on empowerment, rehabilitation and reconnection with the community. When an individual is placed in one of our, we work with them to develop the skills and confidence needed to return to independent living as quickly as possible. We also assist those who are in the process of adjusting to independent living to fast track their process as practically as possible.</p>
									<p>Our aim is to provide mental health care, support and enable individuals to acquire and develop day to day skills. These skills include shopping, personal care, personal hygiene, preparing meals and community skills. Individuals are encouraged to access and participate in local community life through local facilities. Our staff help individuals to access any mainstream activity or hobby they are interested in. Staff will also support and promote equal opportunities, rights, customs and diversity of the individual.</p>
									<p>For more information about t help and support for yourself or someone you care for please contact us on&nbsp;<strong> 020 8665 4334</strong></p>
									</div>
									<button class="accordion"><strong>Elderly People</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
											<p>
												You are likely to have come here because you have a significant decision to make about long term elderly care for you or your loved one. Are you struggling to understand whether care at home or a care home would best suit your needs? Or do you have a parent who is reluctant to accept that they need help? We truly understand how to plan long term care, including financing.
											</p>
											<p>
												<strong>Understanding your care needs</strong><br>
												Before you research any elderly care providers, it is prudent to truly understand the type and level of support you and your family require. For example, do you need a live-in service, or will hourly care suffice? Do you require specialist support because you or your loved one is living with a complex condition, such as dementia or Parkinson’s? How may you be able to access appropriate care for a couple wishing to stay together? Or are you looking for other types of support, for example with your social activities or household duties?
												<p><strong>Evaluating home care</strong><br>
												You may have already decided that you want to stay in your own home for as long as possible, but need some support. There are many compelling benefits to staying in your home, but you may be unsure as to how home care works. Then you have to find a home care provider – how do you evaluate which provider would best suit your needs?We are here to help you make sense of all the options available to you.<br>
												If you would like a free, no obligation consultation with one of our experienced Managers, call us today on <strong>020 8665 4334.</strong>
											</p>
										</div>

									<button class="accordion"><strong>Physical Disabilities Care</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
										<p>
										If you have (or someone living with you has) a physical disability and need assistance to enable you to use essential facilities in your home (bathroom, kitchen, etc.) or to help with personal care, or for accessing the community, there are a number of things you can do. One of your options is for us to provide you with tailor-made support and assistance, delivered by caring professionals enabling you to live life to its full potential.
										</p>
										<p>This support will depend upon your individual situation and follow a comprehensive needs assessment we would offer a range of services to suit you, which might include:</p>
										<p>
											Home Care – help at home with your personal care (such as getting out of bed, washing, dressing)<br>
											Household tasks (such as cleaning, ironing, etc.)<br>
											Shopping<br>
											Help with administering paperwork and bill payment<br>
											Support in accessing community activities
										</p>
										<p>For more information about t help and support for yourself or someone you care for please contact us on <strong>020 8665 4334</strong></p>
										<p>Divine Motions AcaCare Limited provides a range of specialist needs-led services for adults (18-64) with learning disabilities and complex support needs including Autism, Epilepsy, Severe Learning Disabilities, Profound and Multiple Learning Disabilities and High Vulnerability.</p>
										<p>Combining the family perspective with experienced professionals from social care, a dynamic service has evolved that strives to offer individuals with a learning disability better outcomes and wider horizons<br>
										We offer support with personal care, as well as help and support for each person to learn new skills, develop independence, access other services and enjoy life as part of the community.</p>
										<p>We provide home based support to individuals who are referred by Social Services or who are purchasing services through Direct Payments or Individual Budgets.</p>
										<p>Each person has a Support Plan and an Activity Planner. Information includes personal care, learning opportunities, recreational, social, respite breaks, vocational and work activities.</p>
										
									</div>
									<button class="accordion"><strong>Learning Disabilities</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
										<p>Someone with a Learning Disability, such as Down’s Syndrome, dyslexia etc. can live in their own home, retain their independence and make choices about how to live their life through our live in care package. A primary benefit of live in care is the versatility it offers as and when needs change – having a carer living in the same household, means that it is possible for a person to have full-time support or extra support for a while, without changing their living arrangements or introducing new people into their home.</p>
										<p>We aim to support people with learning disabilities to live independently as part of their community, close to friends and family if possible. Having live in carers means that the person can exercise choice and control over their life in ways that would not usually be possible, if living in structured environments such as residential care or group living.</p>
										<p>Our live in carers will listen and try to understand how people are feeling – so they provide the right level of support in the right time and place.</p>
										<p>We completely fit around people’s routines and choices – and deliver flexible support that’s unique to them. Open communication is so important, so we always look for feedback and carry out regular reviews with individuals, their family and circle of support, to ensure they feel comfortable with the support they are receiving.<br>
										For more information about t help and support for yourself or someone you care for please contact us on&nbsp; 020 8665 4334.</p>
									</div>
									
									<button class="accordion"><strong>Palliative Care</strong><i class="fa fa-plus"></i></button>
									<div class="panel">
										It can be a difficult time when a loved one is terminally ill. Our experienced trained Carers and support staff will support the whole family with sensitivity. Our staff will treat your loved one with the dignity and care they deserve.<br>
										We can also bridge the gap prior to the Palliative Care Team being set up to deliver care. We endeavour to accommodate the client and family wishes where possible, and work with and around other agencies that are involved in the care.</p>
										<p>Whatever the situation Divine Motions AcaCare Ltd can always deliver highly trained and sympathetic Palliative Care Personnel for you and your loved ones.</p>
										<p>For more information about t help and support for yourself or someone you care for please contact us on <strong>020 8665 4334.</strong></p>
									</div>
    @endsection