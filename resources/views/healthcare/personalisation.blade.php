@extends('layouts.page')
@section('page-title')
About : CQC Report - Divine Motions Aca Care
@endsection
@section('article-title')
Personalisation
@endsection
@section('article-body')
<button class="accordion"><strong>Mental Health
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
Divine Motions AcaCare Ltd provides a range of community based mental health care services including low secure and step down facilities, supported living, residential care and independent hospitals, for adults with mental health needs.<br>
We support individuals with severe and enduring mental health needs such as alcohol and drug induced psychosis, schizophrenia and bi-polar disorder.<br>
The focus of our mental health care services is on rehabilitation and empowerment. This means that when an individual is placed in one of our services, we work with them to develop the skills and confidence needed to return to independent living as quickly as possible.</p>
</div>
<button class="accordion"><strong>Respite Care
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
</strong>Respite Care is suitable for people in need of rehabilitation following a stay in the hospital or extra care following a period of illness. It also allows the sharing of care giving responsibility and getting support for the main care giver, be it a family member or friend. Seeking support and maintaining one’s own health is paramount and using respite care before becoming exhausted, isolated or overwhelmed is ideal. Just anticipating regular relief can become a lifesaver.</p>
</div>
<button class="accordion"><strong>Learning Disabilities
</strong><i class="fa fa-plus"></i></button>
	<div class="panel">
We are a provider that delivers high quality, flexible and person centred services to people with a learning disability.<br>
We have the right people with the right skills and all our staff really enjoy supporting people to lead fulfilling lives.</p>
</div>
@endsection